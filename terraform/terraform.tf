
provider "aws" {
  region = "us-east-3"
}

resource "aws_instance" "puppetagent1" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "Gitlab_keypair"

  tags = {
    Name = "puppetagent1"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("Gitlab_keypair.pem")
    host        = self.public_ip
    timeout     = "5m"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.28.184 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"


    ]
  }
}

output "public_ip" {
  value = aws_instance.puppetagent1.public_ip
}
